```
Kernel branch: android15-6.6
Kernel version: 6.6.66
Kernel SHA: d424613e906e183995ea3ebed850bfc0d644e625
Build server ID: 13024435
Test device: Pixel 6
```

| Benchmark | Improvement |
|---|---|
| Boot time | 3.1% |
| Cold App launch time | 2.0% |
| Binder-rpc | 15.7% |
| Binder-addints | 15.9% |
| Hwbinder | 11.2% |
